import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import ListItem from './ListItem/ListItem';

export default props => {
    return (
        <FlatList
            style={styles.listContainer}
            data={props.places}
            renderItem={info => (
                <ListItem onItemPressed={() => props.itemSelected(info.item.key)} placeName={info.item.name} placeImage={info.item.image} />
            )}
        />
    );
};

const styles = StyleSheet.create({
    listContainer: {
        width: '100%'
    }
});
