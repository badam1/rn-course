import React, { Component, Fragment } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import Button from '../UI/Button/Button';
import placeImg from '../../assets/beautiful-place.jpg';

class PickImage extends Component {
    render() {
        return (
            <Fragment>
                <View style={styles.placeholder}>
                    <Image source={placeImg} style={styles.previewImage} />
                </View>
                <Button title="Pick Image" color="#29aaf4" textColor="#fff" onPress={() => alert('Pick image')} />
            </Fragment>
        );
    }
}
const styles = StyleSheet.create({
    placeholder: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#eee',
        width: '80%',
        height: 150
    },
    previewImage: {
        width: '100%',
        height: '100%'
    }
});
export default PickImage;
