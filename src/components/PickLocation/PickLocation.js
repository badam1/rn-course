import React, { Component, Fragment } from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import Button from '../UI/Button/Button';

class PickLocation extends Component {
    render() {
        return (
            <Fragment>
                <View style={styles.placeholder}>
                    <Text>Map</Text>
                </View>
                <Button title="Locate me" color="#29aaf4" textColor="#fff" onPress={() => alert('Pick location')} />
            </Fragment>
        );
    }
}
const styles = StyleSheet.create({
    placeholder: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#eee',
        width: '80%',
        height: 150
    }
});
export default PickLocation;
