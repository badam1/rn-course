import React from 'react';
import { TouchableOpacity, TouchableNativeFeedback, Text, View, StyleSheet, Platform } from 'react-native';

const Button = props => {
    const view = (
        <View style={[styles.button, { backgroundColor: props.color }]}>
            <Text style={{ color: props.textColor }}>{props.title}</Text>
        </View>
    );

    if (Platform.OS === 'android') {
        return <TouchableNativeFeedback onPress={props.onPress}>{view}</TouchableNativeFeedback>;
    }
    return <TouchableOpacity onPress={props.onPress}>{view}</TouchableOpacity>;
};
const styles = StyleSheet.create({
    button: {
        padding: 10,
        margin: 5,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'transparent'
    }
});
export default Button;
