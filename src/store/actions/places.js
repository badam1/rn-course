import * as actionTypes from './actionTypes';

export const addPlace = placeName => ({ type: actionTypes.ADD_PLACE, placeName });
export const deletePlace = key => ({ type: actionTypes.DELETE_PLACE, key });
