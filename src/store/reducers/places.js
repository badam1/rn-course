import { ADD_PLACE, DELETE_PLACE } from '../actions/actionTypes';
import updateObject from './utility';

const initialState = {
    places: []
};

const addPlace = (state, action) =>
    updateObject(state, {
        places: state.places.concat({
            key: 'key-' + Math.random(),
            name: action.placeName,
            image: {
                uri: 'https://c1.staticflickr.com/5/4096/4744241983_34023bf303_b.jpg'
            }
        })
    });

const deletePlace = (state, action) =>
    updateObject(state, {
        places: state.places.filter(place => place.key !== action.key)
    });

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_PLACE:
            return addPlace(state, action);
        case DELETE_PLACE:
            return deletePlace(state, action);
        default:
            return state;
    }
};
