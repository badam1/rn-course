import { Dimensions } from 'react-native';
export const PORTRAIT = 'PORTRAIT';
export const LANDSCAPE = 'LANDSCAPE';

export default () => (Dimensions.get('window').height > 500 ? PORTRAIT : LANDSCAPE);
