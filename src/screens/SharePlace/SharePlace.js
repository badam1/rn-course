import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, StyleSheet, ScrollView } from 'react-native';
import { addPlace } from '../../store/actions';
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
import Button from '../../components/UI/Button/Button';
import MainText from '../../components/UI/MainText/MainText';
import HeadingText from '../../components/UI/HeadingText/HeadingText';
import PickImage from '../../components/PickImage/PickImage';
import PickLocation from '../../components/PickLocation/PickLocation';

class SharePlaceScreen extends Component {
    static navigatorStyle = {
        navBarButtonColor: '#29aaf4'
    };
    state = {
        placeName: ''
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    onNavigatorEvent = event => {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'sideDrawerToggle') {
                this.props.navigator.toggleDrawer({
                    side: 'left'
                });
            }
        }
    };

    placeNameChangeHandler = val => this.setState({ placeName: val });

    placeSubmitHandler = () => {
        if (this.state.placeName.trim() === '') return;
        this.props.addPlace(this.state.placeName);
        this.setState({ placeName: '' });
    };

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <MainText>
                        <HeadingText style={styles.heading}>Share A Place With Us!</HeadingText>
                    </MainText>
                    <PickImage />
                    <PickLocation />
                    <DefaultInput
                        placeholder="Place name!"
                        onChangeText={this.placeNameChangeHandler}
                        value={this.state.placeName}
                    />
                    <Button
                        title="Share The Place!"
                        color="#29aaf4"
                        textColor="#fff"
                        onPress={this.placeSubmitHandler}
                    />
                </View>
            </ScrollView>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addPlace: placeName => dispatch(addPlace(placeName))
});

const styles = StyleSheet.create({
    heading: {
        color: 'black'
    },
    container: {
        flex: 1,
        alignItems: 'center'
    }
});

export default connect(null, mapDispatchToProps)(SharePlaceScreen);
