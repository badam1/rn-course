import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default () => {
    Promise.all([
        Icon.getImageSource(`${Platform.OS === 'android' ? 'md' : 'ios'}-share-alt`, 30),
        Icon.getImageSource(`${Platform.OS === 'android' ? 'md' : 'ios'}-map`, 30),
        Icon.getImageSource(`${Platform.OS === 'android' ? 'md' : 'ios'}-menu`, 30)
    ]).then(sources => {
        const [sharePlaceIcon, findPlaceIcon, menuIcon] = sources;
        Navigation.startTabBasedApp({
            tabs: [
                {
                    screen: 'rncourse.FindPlaceScreen',
                    label: 'Find place',
                    title: 'Find place',
                    icon: findPlaceIcon,
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: menuIcon,
                                title: 'Menu',
                                id: 'sideDrawerToggle'
                            }
                        ]
                    }
                },
                {
                    screen: 'rncourse.SharePlaceScreen',
                    label: 'Share place',
                    title: 'Share place',
                    icon: sharePlaceIcon,
                    navigatorButtons: {
                        leftButtons: [
                            {
                                icon: menuIcon,
                                title: 'Menu',
                                id: 'sideDrawerToggle'
                            }
                        ]
                    }
                }
            ],
            tabsStyle: {
                tabBarSelectedButtonColor: '#29aaf4'
            },
            appStyle: {
                tabBarSelectedButtonColor: '#29aaf4'
            },
            drawer: {
                left: {
                    screen: 'rncourse.SideDrawer'
                }
            }
        });
    });
};
