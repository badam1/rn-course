import React, { Component } from 'react';
import { View, ImageBackground, StyleSheet, Dimensions } from 'react-native';
import startMainTabs from '../MainTabs/startMainTabs';
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
import HeadingText from '../../components/UI/HeadingText/HeadingText';
import MainText from '../../components/UI/MainText/MainText';
import Button from '../../components/UI/Button/Button';
import bgImg from '../../assets/background.jpg';
import getViewMode, { PORTRAIT, LANDSCAPE } from '../../utils/utils';

class AuthScreen extends Component {
    state = {
        viewMode: getViewMode()
    };

    constructor(props) {
        super(props);
        Dimensions.addEventListener('change', this.updateStyles);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.updateStyles);
    }

    updateStyles = dims => this.setState({ viewMode: getViewMode() });

    loginHandler = () => startMainTabs();

    render() {
        let headingText = null;
        if (getViewMode() === PORTRAIT) {
            headingText = (
                <MainText>
                    <HeadingText>Please Log In</HeadingText>
                </MainText>
            );
        }
        return (
            <ImageBackground source={bgImg} style={styles.backgroundImage}>
                <View style={styles.container}>
                    {headingText}
                    <Button title="Switch to Login" color="#29aaf4" textColor="#fff" />
                    <View style={styles.inputContainer}>
                        <DefaultInput placeholder="Your E-mail address" />
                        <View
                            style={
                                this.state.viewMode === PORTRAIT
                                    ? styles.portraitPasswordContainer
                                    : styles.landscapePasswordContainer
                            }
                        >
                            <View
                                style={
                                    this.state.viewMode === PORTRAIT
                                        ? styles.portraitPasswordWrapper
                                        : styles.landscapePasswordWrapper
                                }
                            >
                                <DefaultInput placeholder="Password" />
                            </View>
                            <View
                                style={
                                    this.state.viewMode === PORTRAIT
                                        ? styles.portraitPasswordWrapper
                                        : styles.landscapePasswordWrapper
                                }
                            >
                                <DefaultInput placeholder="Confirm Password" />
                            </View>
                        </View>
                    </View>
                    <Button title="Submit" onPress={this.loginHandler} color="#29aaf4" textColor="#fff" />
                </View>
            </ImageBackground>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundImage: {
        width: '100%',
        flex: 1
    },
    inputContainer: {
        width: '80%'
    },
    landscapePasswordContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    portraitPasswordContainer: {
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    landscapePasswordWrapper: {
        width: '45%'
    },
    portraitPasswordWrapper: {
        width: '100%'
    }
});
export default AuthScreen;
