import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Image, Text, Button, StyleSheet, TouchableOpacity, Platform, Dimensions } from 'react-native';
import { deletePlace } from '../../store/actions';
import Icon from 'react-native-vector-icons/Ionicons';
import getViewMode, { PORTRAIT, LANDSCAPE } from '../../utils/utils';

class PlaceDetailScreen extends Component {
    state = {
        viewMode: getViewMode()
    };
    constructor(props) {
        super(props);
        Dimensions.addEventListener('change', this.updateStyle);
    }

    componentWillUnmount() {
        Dimensions.removeEventListener('change', this.updateStyle);
    }

    updateStyle = dims => this.setState({ viewMode: getViewMode() });

    deletePlaceHandler = () => {
        this.props.onDeletePlace(this.props.selectedPlace.key);
        this.props.navigator.pop();
    };
    render() {
        let image = <Image source={this.props.selectedPlace.image} style={styles.portraitPlaceImage} />;
        let text = <Text style={styles.portraitPlaceName}>{this.props.selectedPlace.name}</Text>;
        if (this.state.viewMode === LANDSCAPE) {
            image = <Image source={this.props.selectedPlace.image} style={styles.landscapePlaceImage} />;
            text = null;
        }
        return (
            <View style={styles.container}>
                {text}
                <View style={this.state.viewMode === LANDSCAPE ? styles.landscapeWrapper : null}>{image}</View>
                <TouchableOpacity onPress={this.deletePlaceHandler}>
                    <View style={styles.deleteBtn}>
                        <Icon size={30} name={`${Platform.OS === 'android' ? 'md' : 'ios'}-trash`} color="red" />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 22
    },
    portraitPlaceImage: {
        marginRight: 8,
        height: 200,
        width: '100%'
    },
    landscapePlaceImage: {
        marginRight: 8,
        height: 150,
        width: '100%'
    },
    portraitPlaceName: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 28
    },
    landscapePlaceName: {
        fontWeight: 'bold',
        fontSize: 22
    },
    deleteBtn: {
        alignItems: 'center'
    },
    landscapeWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});
const mapDispatchToProps = dispatch => ({
    onDeletePlace: key => dispatch(deletePlace(key))
});

export default connect(null, mapDispatchToProps)(PlaceDetailScreen);
