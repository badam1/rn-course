import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import Button from '../../components/UI/Button';

import List from '../../components/List/List';

class FindPlaceScreen extends Component {
    static navigatorStyle = {
        navBarButtonColor: '#29aaf43'
    };

    state = {
        placesLoaded: false
    };
    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    onNavigatorEvent = event => {
        if (event.type === 'NavBarButtonPress') {
            if (event.id === 'sideDrawerToggle') {
                this.props.navigator.toggleDrawer({
                    side: 'left'
                });
            }
        }
    };

    itemSelectedHandler = key => {
        const selPlace = this.props.places.find(place => place.key === key);
        this.props.navigator.push({
            screen: 'rncourse.PlaceDetailScreen',
            title: selPlace.name,
            passProps: {
                selectedPlace: selPlace
            }
        });
    };

    placesSearchHandler = () => this.setState({ placesLoaded: false });

    render() {
        let content = this.state.placesLoaded ? (
            <List places={this.props.places} itemSelected={this.itemSelectedHandler} />
        ) : (
            <Button title="Find Places" color="orange" onPress={this.placesSearchHandler} />
        );
        return <View>{content}</View>;
    }
}
const mapStateToProps = state => ({
    places: state.places.places
});

export default connect(mapStateToProps)(FindPlaceScreen);
